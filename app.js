"use strict";
// load env variable from .env file
const dotenv = require("dotenv");
dotenv.config();
const createError = require("http-errors");
const express = require("express");
const path = require("path");
const logger = require("morgan");
const router = require("./routes/index");
const knexInit = require("knex");
const Model = require("objection").Model;
const swagger_loader = require("./loaders/swagger_loader");

const app = express();

// load database
const knexConfig = require("./config/knexfile");
const knex = knexInit(knexConfig[process.env.NODE_ENV]);

// load objection
Model.knex(knex);
// email_loader.load(app)
swagger_loader.load(app);

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));

// var bodyParser = require("body-parser");
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: true }));
router.init(app);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  // res.locals.error = req.app.get('env') === 'development' ? err : {}
  // TODO i enabled this for staging but will be disabled later
  res.locals.error = req.app.get("env") === "development" ? err : err; // disable this in future or use it with env variable

  // render the error page
  res.status(err.status || 555);
  res.render("error");
});

module.exports = app;
