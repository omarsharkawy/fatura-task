exports.up = function (knex) {
  return knex.schema.createTable("categories", function (table) {
    table.increments("id").primary();
    table.string("name", 255).notNullable().unique();
    table.integer("parent_id").unsigned();
    table.foreign("parent_id").references("id").inTable("categories");
  });
};

exports.down = function (knex) {
  return knex.schema.dropTableIfExists("categories");
};
