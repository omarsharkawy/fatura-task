exports.up = function (knex) {
  return knex.schema.createTable("products", function (table) {
    table.increments("id").primary();
    table.string("name", 255).notNullable().unique();
    table.string("image_uri", 255);
    table.boolean("featured");
    table.integer("category_id").unsigned().notNullable();
    table.foreign("category_id").references("id").inTable("categories");
  });
};

exports.down = function (knex) {
  return knex.schema.dropTableIfExists("products");
};
