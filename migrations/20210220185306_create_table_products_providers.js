exports.up = function (knex) {
  return knex.schema.createTable("products_providers", function (table) {
    table.increments("id").primary();
    table.decimal("price").notNullable();
    table.boolean("available").defaultTo(false);
    table.integer("product_id").unsigned().notNullable();
    table.foreign("product_id").references("id").inTable("products");
    table.integer("provider_id").unsigned().notNullable();
    table.foreign("provider_id").references("id").inTable("providers");
  });
};

exports.down = function (knex) {
  return knex.schema.dropTableIfExists("products_providers");
};
