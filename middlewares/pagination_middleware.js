async function pagination(req, res, next) {
  var page = parseInt(req.params.page) || 1;
  var offset = (page - 1) * 25 == 0 ? 1 : (page - 1) * 25;
  var limit = page * 25;
  req.offset = offset;
  req.limit = limit;
  next();
}

module.exports = {
  pagination,
};
