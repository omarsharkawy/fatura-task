"use strict";

const Model = require("objection").Model;

class Product extends Model {
  // Table name is the only required property.
  static get tableName() {
    return "products";
  }

  static getProduct(offset, limit, category) {
    return Product.query()
      .select("*")
      .where("category", "=", category)
      .offset(offset)
      .limit(limit)
      .eager("[category,provider]");
  }

  static createProduct(product) {
    return Product.query().insert({
      name: product.name,
      image_uri: product.image_uri,
      featured: prodcut.featured,
      category_id: product.category_id,
    });
  }

  static setProductFeatured(id, featured) {
    return Product.query().update({
      id: id,
      featured: featured,
    });
  }

  static get relationMappings() {
    const Category = require("./category");
    const Provider = require("./provider");
    const Product_Provider = require("./prodcuts_providers");
    return {
      category: {
        relation: Model.BelongsToOneRelation,
        modelClass: Category,
        join: {
          from: "products.category_id",
          to: "categories.id",
        },
      },
      provider: {
        relation: Model.ManyToManyRelation,
        modelClass: Provider,
        filter: (query) => query.select("providers.name"),
        join: {
          from: "providers.id",
          through: {
            modelClass: Product_Provider,
            filter: (query) =>
              query
                .select(
                  "products_providers.price, products_providers.availability"
                )
                .max("products_providers.price"),
            from: "products_providers.product_id",
            to: "products_providers.provider_id",
          },
          to: "products.id",
        },
      },
    };
  }
}

module.exports = Product;
