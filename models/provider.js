"use strict";

const Model = require("objection").Model;

class Provider extends Model {
  // Table name is the only required property.
  static get tableName() {
    return "providers";
  }

  static createProduct(provider) {
    return Provider.query().insert({
      name: provider.name,
    });
  }
}

module.exports = Provider;
