"use strict";

const Model = require("objection").Model;

class ProductsProviders extends Model {
  // Table name is the only required property.
  static get tableName() {
    return "products_providers";
  }

  static createProductProvider(product_provider) {
    return Product.query().insert({
      price: product_provider.price,
      available: product_provider.available,
      product_id: product_provider.product_id,
      provider_id: product_provider.provider_id,
    });
  }
}

module.exports = ProductsProviders;
