"use strict";

const Model = require("objection").Model;

class Category extends Model {
  // Table name is the only required property.
  static get tableName() {
    return "categories";
  }

  static createCategory(category) {
    return Category.query().insert({
      name: category.name,
      patient_id: category.patient_id,
    });
  }

  static get relationMappings() {
    return {
      parent: {
        relation: Model.BelongsToOneRelation,
        modelClass: Category,
        join: {
          from: "categories.parent_id",
          to: "categories.id",
        },
      },
    };
  }
}

module.exports = Category;
