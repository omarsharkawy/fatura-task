const ProductService = require("../services/products");

const getProducts = async function (req, res, next) {
  try {
    const valid_req = req.limit && req.offset && req.query.category;
    if (!valid_req) {
      res.status(400).json({ status: "Failure", message: "incorrect request" });
    }

    const Products = await ProductService.getProducts({
      offset: req.offset,
      limit: req.limit,
      category: req.query.category,
    });

    return res.status(200).json({
      status: "success",
      products: Products,
    });
  } catch (e) {
    return next(e);
  }
};

const setProductFeatured = async function (req, res, next) {
  try {
    const valid_req = req.params.id;
    if (!valid_req) {
      res.status(400).json({ status: "Failure", message: "incorrect request" });
    }

    await ProductService.setProductFeatured({
      id: req.params.id,
      featured: req.body.featured,
    });

    return res.status(200).json({
      status: "success",
    });
  } catch (e) {
    return next(e);
  }
};

module.exports = {
  getProducts,
  setProductFeatured,
};
