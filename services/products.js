const ProductsModel = require("../models/product");

async function getProducts({ offset, limit, category }) {
  const products = await ProductsModel.getProducts(offset, limit, category);

  return products;
}

async function setProductFeatured(id, featured) {
  const result = await ProductsModel.setProductFeatured(id, featured);

  return result;
}

module.exports = {
  getProducts,
  setProductFeatured,
};
