const healthRouter = require("./health");
const products = require("./products");

module.exports.init = function (app) {
  app.use("/api", healthRouter.router);
  app.use("/api", products.router);
};
