const express = require("express");
const productsController = require("../controllers/product_controller");
const router = express.Router();
const { pagination } = require("../middlewares/pagination_middleware");

router.get("/products/:page", pagination, productsController.getProducts);
router.put("/products/:id", productsController.setProductFeatured);

module.exports = {
  router,
};
