const express = require('express')
const healthController = require('../controllers/health_controller')
const router = express.Router()

router.get('/health_check', healthController.healthCheck)
module.exports = {
  router
}
