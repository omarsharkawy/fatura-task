# Fatura Task Backend

Excuse me I didn't have the chance to finish it as I would hoped.

there is a file called sql_requested which contains the required sql DDL

this repo contains the Fatura Task backend built with node, express and postgres

## Getting Started

folder structure is inspired by https://softwareontheroad.com/ideal-nodejs-project-structure/ with some modifications like adding controllers folder

### Prerequisites

in order to install this repo you need to make sure you have git, node and postgres installed on your machine

### Installing

A step by step series of examples that tell you how to get a development env running

clone this repo

open terminal and change directory to the project root

install project dependencies

```
npm install
```

create .env in project root directory and copy its data from env.example

```
cp env.example .env
```

create database with whatever name you want in postgres
fill out the .env file with the name for database you chose and also change the other env variable according the description provided in `env.example`

run migrations

```
knex migrate:latest
```

run the project

```
npm run dev
```

TODO create health check and make script that make sure code is working

## Running the tests

UNDER CONSTRUCTION

### Break down into end to end tests

UNDER CONSTRUCTION

### And coding style tests

UNDER CONSTRUCTION

## Deployment

UNDER CONSTRUCTION

## Built With

- [Express](https://expressjs.com/) - The web framework used
- [Knex](https://knexjs.org/) - Database migration
- [Knex Cheatsheet](https://devhints.io/knex) - Database migration
- [Objectionjs](https://vincit.github.io/objection.js/) - Database ORM
- [npm](https://www.npmjs.com/) - Dependency Management
- [Postgres](https://www.postgresql.org/) - Relational Database Management server

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags).

## Authors

- **Omar Sharkawy**
