module.exports = {
  swagger: "2.0",
  explorer: true,
  securityDefinitions: {
    name: "bearer",
    schema: {
      type: "apiKey",
      in: "header",
      name: "Authorization",
      description: "",
    },
    value: "Bearer <my own JWT token>",
  },
  info: {
    version: "1.0.0",
    title: "Fatura Task Swagger",
    description: "backend of Fatura Task",
  },
  host: `${process.env.SWAGGER_HOST}`,
  basePath: "/",
  components: {},
  security: {
    bearer: [],
  },
  tags: [],
  consumes: ["application/json"],
  produces: ["application/json"],
  paths: {},

  definitions: {},
};
